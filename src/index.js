import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import WebStatus from "./WebStatus";

ReactDOM.render(<WebStatus />, document.getElementById("root"));
