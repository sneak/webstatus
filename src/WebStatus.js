import React from "react";
//import logo from './logo.svg';
import "./WebStatus.css";

class WebStatusImage extends React.Component {
    render() {
        return <img className="webstatusimage" src={this.props.url} />;
    }
}

class WebStatus extends React.Component {
    render() {
        const currentImageUrl = "http://placekitten.com/3840/2160";

        return (
            <div className="webstatus">
                <WebStatusImage url={currentImageUrl} />
            </div>
        );
    }
}

export default WebStatus;
