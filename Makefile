
build: node_modules
	yarn build

node_modules:
	yarn install

clean:
	rm -rf build node_modules
