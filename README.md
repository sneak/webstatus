# webstatus

* this is a clientside js web application to consume a directory of static
  images on a server and display them in a rotation.

# homepage

* https://git.eeqj.de/sneak/webstatus

# todo

* nice transitions
* detect how many px '100%' actually is and don't rescale tiny images beyond
  some reasonable measure, so small images can be displayed properly
* allow for text rendering inside the manifest maybe

# license

WTFPL

# author

sneak &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;
