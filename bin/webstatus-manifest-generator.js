#!/usr/bin/env node

"use strict";

const figlet = require("figlet");
const chalk = require("chalk");
const fs = require("fs");
const find = require("find");
const pkg = require("../package.json");

const warnSymbol = "⚠️";
const headExplode = "🤯";
const rightArrow = "➡️";

function pprint(obj) {
    console.log("%o", obj);
}

function outputManifest(path, fileList) {
    if (!fileList) {
        l = [];
    }
    const output = {
        $id: "berlin.sneak.ns.webstatus.manifest-v1",
        manifest: fileList
    };
    fs.writeFileSync(path + ".tmp", JSON.stringify(output));
    fs.renameSync(path + ".tmp", path);
}

function main() {
    const commander = require("commander");
    const program = new commander.Command();
    program.version(pkg.version, "-v --version", "output current version");
    program
        .option("-s --source <directory>", "directory to scan/write", ".")
        .option("-v --verbose", "verbose output", false)
        .option("-q --quiet", "no output", false);

    program.parse(process.argv);

    const print = x => {
        if (program.quiet) {
            return;
        }
        console.log(x);
    };

    const log = {};

    log.info = x => {
        print(rightArrow + chalk.blue("  " + x));
    };

    log.die = x => {
        print(warnSymbol + chalk.bold.red("  " + x));
        print(headExplode);
        process.exit(-1);
    };

    log.huge = x => {
        var f = figlet.textSync(x, {
            font: "Red Phoenix",
            horizontalLayout: "default",
            verticalLayout: "default"
        });
        print(chalk.red(f));
    };

    log.huge("webstatus");
    //const dir = program.source
    //pprint(program)
    if (!fs.lstatSync(program.source).isDirectory()) {
        log.die(`${program.source} is not a directory`);
    } else {
        process.chdir(program.source);
        program.source = process.cwd();
        log.info(`scanning ${program.source} for images...`);
    }

    var fileList = [];
    var manifestFilename = program.source + "/webstatus.manifest.json";

    const matchRE = /\.(png|gif|jpg|jpeg|webp)$/i;

    var candidateList = find.fileSync(matchRE, program.source);

    candidateList.forEach(fn => {
        var stat = fs.statSync(fn);
        var prefix = new RegExp("^" + program.source + "/");
        fn = fn.replace(prefix, "");
        fileList.push({
            // FIXME read image size from files so that
            // renderer can scale them more intelligently
            $id: "berlin.sneak.ns.webstatus.fileitem-v1",
            path: fn,
            mtime: stat.mtime
        });
    });
    log.info(
        `writing ${fileList.length} entries to manifest ${manifestFilename}`
    );
    try {
        outputManifest(manifestFilename, fileList);
    } catch (err) {
        program.quiet = false;
        log.die(`unable to write manifest: ${err}`);
    }
    log.info("success");
}

main();
